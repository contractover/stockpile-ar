/* const.vala
 *
 * Copyright 2023 skøldis
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

namespace Jellybean {
	public class Jellybeans {
		[Description (nick = "Name of the jellybean")]
		// Translators: this is the name of new created items
		public string name;

		[Description (nick = "Number of stock left, in units")]
		public double number;

		[Description (nick = "Units used for the jellybean")]
		// Translators: use the same noun as with the "New Item" translation for consistency
		public string unit;

		[Description (nick = "Number which is the threshold for being marked as low in stock, in units")]
		public double low;

		[Description (nick = "Default amount to use and refill, in units")]
		public double quick;

		[Description (nick = "Icon assosciated with jellybean")]
		public int icon ;

		[Description (nick = "Create a jellybean from properties")]
		public static Jellybeans from_props (
		    string title = _("New Item"), double number = 50,
		    string unit = "", double low_stock = 10,
		    double quick_use = 1, int icon = 0
		) {
            Jellybean.Jellybeans jlb = new Jellybean.Jellybeans ();
			jlb.name = title;
			jlb.number = number;
			jlb.unit = unit;
			jlb.low = low_stock;
			jlb.quick = quick_use;
			jlb.icon = icon;
            return jlb;
		}
	}

	public Jellybean.Func show_func;

	public class IconButton : Gtk.Button {
		public int icon { get; protected set; }

		public IconButton (int enum_num) {
            enum_num = (enum_num < 6) ? enum_num : 0;
			icon = enum_num;
			this.icon_name = ICON_NAMES[enum_num] ?? "ticket";
			this.name = enum_num.to_string ();
            this.tooltip_text = ICON_TOOLTIPS[enum_num];
			this.add_css_class ("circular");
			this.add_css_class ("flat");
			this.valign = Gtk.Align.CENTER;
		}
	}

	public const string[] ICON_NAMES = {
		"ticket", "document", "bread", "cafe", "pill", "wallet"
	};

    public string[] ICON_TOOLTIPS;

	public class StringSearchTerms : Object {
		private string[] string0 = {
			// Translators: This is a search term for the ticket icon
			_("Ticket"), _("Pass")
		};

		private string[] string1 = {
			// Translators: This is a search term for the document icon
			_("Paper"), _("Document"), _("Printed")
		};

		private string[] string2 = {
			// Translators: This is a search term for the bread icon
			_("Bread"), _("Food"), _("Groceries")
		};

		private string[] string3 = {
			// Translators: This is a search term for the coffee icon
			_("Cafe"), "Café", _("Coffee"), _("Drink")
		};

		private string[] string4 = {
			// Translators: This is a search term for the pill icon
			_("Medicine"), _("Pill"), _("Treatment")
		};

		private string[] string5 = {
			// Translators: This is a search term for the wallet icon
			_("Wallet"), _("Money"), _("Bank")
		};

		public string[] find_for_icon (int icon) {
			switch (icon) {
			case 1:
				return string1;
			case 2:
				return string2;
			case 3:
				return string3;
			case 4:
				return string4;
			case 5:
				return string5;
			case 0:
			default:
				return string0;
			}
		}

		public bool match_with_regex (int icon, Regex regex) {
			string[] list = find_for_icon (icon);

			foreach (string item in list) {
				if (regex.match (item)) return true;
			}

			return false;
		}
	}
}
