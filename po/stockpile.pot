# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the stockpile package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: stockpile\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-12-12 22:16+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: data/app.desktop:3 data/garden.turtle.Jellybean.metainfo.xml.in:7
#: src/application.vala:74 src/ui/main.blp:30 src/ui/main.blp:35
msgid "Stockpile"
msgstr ""

#: data/app.desktop:9
msgid "Stockpile;Jellybean;Inventory;Number;"
msgstr ""

#: data/garden.turtle.Jellybean.metainfo.xml.in:8
msgid "Keep count of restockable items"
msgstr ""

#: data/garden.turtle.Jellybean.metainfo.xml.in:11
msgid ""
"Never forget to refill again! Stockpile lets you manage your inventory of "
"medication, food, beauty products, or anything else. The app will warn you "
"whenever you are short on something, so you can obtain more and not have to "
"go empty."
msgstr ""

#: data/garden.turtle.Jellybean.metainfo.xml.in:42
msgid "Changed app name from “Jellybean” to “Stockpile”"
msgstr ""

#: data/garden.turtle.Jellybean.metainfo.xml.in:43
msgid "Updated icon to match name"
msgstr ""

#: data/garden.turtle.Jellybean.metainfo.xml.in:44
msgid "Reduce user effort by removing unnessescary dialogs and toast"
msgstr ""

#: data/garden.turtle.Jellybean.metainfo.xml.in:45
msgid "Display item amount in the main view"
msgstr ""

#: data/garden.turtle.Jellybean.metainfo.xml.in:46
msgid "Reorder buttons and inputs across the app to make more sense"
msgstr ""

#: data/garden.turtle.Jellybean.metainfo.xml.in:47
msgid "Display item icons on the item page"
msgstr ""

#: data/garden.turtle.Jellybean.metainfo.xml.in:49
msgid ""
"If you are having trouble updating the app, please reinstall and select to "
"keep your user data, and Stockpile should launch correctly afterwards."
msgstr ""

#: data/garden.turtle.Jellybean.metainfo.xml.in:59
msgid "Added Icelandic translation"
msgstr ""

#: data/garden.turtle.Jellybean.metainfo.xml.in:60
#: data/garden.turtle.Jellybean.metainfo.xml.in:84
msgid "Fix UI bugs"
msgstr ""

#: data/garden.turtle.Jellybean.metainfo.xml.in:67
msgid "2-pane UI removed to improve clarity of use"
msgstr ""

#: data/garden.turtle.Jellybean.metainfo.xml.in:68
msgid "Added feedback on jellybean delete"
msgstr ""

#: data/garden.turtle.Jellybean.metainfo.xml.in:69
msgid "Icons can now be set for each item"
msgstr ""

#: data/garden.turtle.Jellybean.metainfo.xml.in:70
msgid "Items can now be searched for"
msgstr ""

#: data/garden.turtle.Jellybean.metainfo.xml.in:71
msgid "Fixed various bugs"
msgstr ""

#: data/garden.turtle.Jellybean.metainfo.xml.in:73
msgid ""
"This release updates the backend. Do not export data from this version to "
"another version. If you were using a lower version, please run this version "
"first before using a newer version."
msgstr ""

#: data/garden.turtle.Jellybean.metainfo.xml.in:83
msgid "Added German, Catalan, French, Hungarian, and Italian translations"
msgstr ""

#: data/garden.turtle.Jellybean.metainfo.xml.in:91
msgid "Switched to 2-pane interface in larger window sizes"
msgstr ""

#: data/garden.turtle.Jellybean.metainfo.xml.in:92
msgid ""
"Removed destructive action confirmation dialogs, instead replacing them with "
"toasts with an Undo action"
msgstr ""

#: data/garden.turtle.Jellybean.metainfo.xml.in:93
msgid "Added better tooltips to many buttons and improved keyboard shortcuts"
msgstr ""

#: data/garden.turtle.Jellybean.metainfo.xml.in:94
msgid "Removed the “Quick-Use” button"
msgstr ""

#: data/garden.turtle.Jellybean.metainfo.xml.in:101
msgid "Added localized window title"
msgstr ""

#: data/garden.turtle.Jellybean.metainfo.xml.in:102
msgid "Adjusted icon"
msgstr ""

#: data/garden.turtle.Jellybean.metainfo.xml.in:109
msgid "Added all basic functionality"
msgstr ""

#: data/garden.turtle.Jellybean.metainfo.xml.in:110
msgid "Added translation to Swedish"
msgstr ""

#. Translators: do one of the following, one per line: Your Name, Your Name <email@email.org>, Your Name https://websi.te
#: src/application.vala:86
msgid "translator-credits"
msgstr ""

#: src/application.vala:93
msgid "Contributors"
msgstr ""

#. Contributors: do one of the following, one per line: Your Name, Your Name <email@email.org>, Your Name https://websi.te
#: src/application.vala:97
msgid "Other Helpers"
msgstr ""

#. Add actions
#. Since gettext does not work without an init outside a window, we will do this here.
#. Translators: This is a search term for the ticket icon
#: src/window.vala:108 src/const.vala:86
msgid "Ticket"
msgstr ""

#. Translators: This is a search term for the document icon
#: src/window.vala:108 src/const.vala:91
msgid "Paper"
msgstr ""

#. Translators: This is a search term for the bread icon
#: src/window.vala:108 src/const.vala:96
msgid "Bread"
msgstr ""

#. Translators: This is a search term for the coffee icon
#: src/window.vala:108 src/const.vala:101
msgid "Cafe"
msgstr ""

#. Translators: This is a search term for the pill icon
#: src/window.vala:108 src/const.vala:106
msgid "Medicine"
msgstr ""

#. Translators: This is a search term for the wallet icon
#: src/window.vala:108 src/const.vala:111
msgid "Wallet"
msgstr ""

#. New jellybean → currently shown jellybean
#. Replace jellybean (or add new)
#. Recompile jellybeans
#. Update rows
#. or just update one row
#. If the jellybean is new, don't do anything more. Return to main view.
#: src/window.vala:206
#, c-format
msgid "Item saved as %s"
msgstr ""

#. Translators: Low stock: is used in both main and item views
#: src/window.vala:217 src/window.vala:282 src/window.vala:377
#: src/window.vala:456
msgid "Low stock: "
msgstr ""

#. Translators: @d %s → for example, 23 units or 495 units. Make sure this works when %s is blank. Double spaces are fine
#: src/window.vala:226 src/window.vala:291 src/window.vala:385
#: src/window.vala:464
#, c-format
msgid "@d %s left"
msgstr ""

#. Deactivate use button if cannot use
#. Item is saved
#. and not new
#: src/window.vala:234
#, c-format
msgid "Item updated as %s"
msgstr ""

#: src/window.vala:258
#, c-format
msgid "Changes to %s discarded"
msgstr ""

#: src/window.vala:259
msgid "Changes to item discarded"
msgstr ""

#: src/window.vala:260
msgid "Undo"
msgstr ""

#.
#. Adw.MessageDialog msg = new Adw.MessageDialog (
#. this,
#. // Translators: %s is the jellybean's name
#. _("Use %s").printf (jellybean.name),
#. // Translators: %s of @s → units of Item
#. _("How many %s of @s would you like to use?").printf (jellybean.unit).replace ("@s", jellybean.name)
#. );
#. msg.close_response = "cancel";
#. msg.add_response ("cancel", _("Cancel"));
#. // Translators: Imperative for using changes
#. msg.add_response ("use", _("Use"));
#. msg.set_response_appearance ("use", Adw.ResponseAppearance.SUGGESTED);
#.
#. double num;
#. num = jellybean.number + 1;
#. if (jellybean.number <= 1) num = jellybean.number;
#. if (jellybean.number == 0) return;
#.
#. Gtk.Adjustment adj = new Gtk.Adjustment (jellybean.quick, 1, num, 1, 10, 1);
#. Gtk.SpinButton spb = new Gtk.SpinButton (adj, 10, 0);
#. msg.set_extra_child (spb);
#.
#. msg.response.connect ((response) => {
#. if (response == "cancel") return;
#. else {
#. If there is not enough left, warn the user and do not follow through
#. Translators: %s → jellybean unit
#: src/window.vala:345
#, c-format
msgid "Not enough %s left"
msgstr ""

#: src/window.vala:489 src/window.vala:496 src/const.vala:45 src/ui/main.blp:52
#: src/ui/main.blp:246
#, c-format
msgid "New Item"
msgstr ""

#: src/window.vala:496 src/window.vala:519
#, c-format
msgid "Editing %s"
msgstr ""

#: src/window.vala:532
msgid "Delete Item?"
msgstr ""

#: src/window.vala:533
msgid "If you delete this item, its information will be deleted permanently."
msgstr ""

#: src/window.vala:537
msgid "_Cancel"
msgstr ""

#. Translators: Imperative for discarding changes
#: src/window.vala:539
msgid "_Delete"
msgstr ""

#. Do nothing if action canceled
#. Delete selected item
#. Full update of rows
#. We have confirmed changes…
#. …so we can leave the page…
#: src/window.vala:549
msgid "Item deleted"
msgstr ""

#: src/util.vala:31
msgid "Item 1"
msgstr ""

#: src/const.vala:86
msgid "Pass"
msgstr ""

#: src/const.vala:91
msgid "Document"
msgstr ""

#: src/const.vala:91
msgid "Printed"
msgstr ""

#: src/const.vala:96
msgid "Food"
msgstr ""

#: src/const.vala:96
msgid "Groceries"
msgstr ""

#: src/const.vala:101
msgid "Coffee"
msgstr ""

#: src/const.vala:101
msgid "Drink"
msgstr ""

#: src/const.vala:106
msgid "Pill"
msgstr ""

#: src/const.vala:106
msgid "Treatment"
msgstr ""

#: src/const.vala:111
msgid "Money"
msgstr ""

#: src/const.vala:111
msgid "Bank"
msgstr ""

#: src/ui/main.blp:44
msgid "Search"
msgstr ""

#: src/ui/main.blp:60
msgid "Main Menu"
msgstr ""

#: src/ui/main.blp:99
msgid "No Results Found"
msgstr ""

#: src/ui/main.blp:101
msgid "Try a different search term"
msgstr ""

#: src/ui/main.blp:122
msgid "Edit Item"
msgstr ""

#: src/ui/main.blp:129
msgid "Delete Item"
msgstr ""

#: src/ui/main.blp:170
msgid "_Use"
msgstr ""

#: src/ui/main.blp:181
msgid "_Refill"
msgstr ""

#: src/ui/main.blp:206
msgid "Cancel"
msgstr ""

#: src/ui/main.blp:212
msgid "Save"
msgstr ""

#: src/ui/main.blp:244
msgid "Name"
msgstr ""

#: src/ui/main.blp:250
msgid "Icon"
msgstr ""

#: src/ui/main.blp:266
msgid "Amount"
msgstr ""

#: src/ui/main.blp:272
msgid "Low Stock Amount"
msgstr ""

#: src/ui/main.blp:278
msgid "Usage Amount"
msgstr ""

#: src/ui/main.blp:284
msgid "Unit (Optional)"
msgstr ""

#: src/ui/main.blp:312
msgid "_Keyboard Shortcuts"
msgstr ""

#: src/ui/main.blp:317
msgid "_About Stockpile"
msgstr ""

#: src/ui/help-overlay.blp:11
msgctxt "shortcut window"
msgid "Main View"
msgstr ""

#: src/ui/help-overlay.blp:15
msgctxt "shortcut window"
msgid "New Item"
msgstr ""

#: src/ui/help-overlay.blp:21
msgctxt "shortcut window"
msgid "Item View"
msgstr ""

#: src/ui/help-overlay.blp:25
msgctxt "shortcut window"
msgid "Use Item"
msgstr ""

#: src/ui/help-overlay.blp:30
msgctxt "shortcut window"
msgid "Refill Item"
msgstr ""

#: src/ui/help-overlay.blp:35
msgctxt "shortcut window"
msgid "Edit Item"
msgstr ""

#: src/ui/help-overlay.blp:40
msgctxt "shortcut window"
msgid "Delete Item"
msgstr ""

#: src/ui/help-overlay.blp:46
msgctxt "shortcut window"
msgid "Edit View"
msgstr ""

#: src/ui/help-overlay.blp:50
msgctxt "shortcut window"
msgid "Save Item"
msgstr ""

#: src/ui/help-overlay.blp:55
msgctxt "shortcut window"
msgid "Discard Item"
msgstr ""

#: src/ui/help-overlay.blp:61
msgctxt "shortcut window"
msgid "General"
msgstr ""

#: src/ui/help-overlay.blp:69
msgctxt "shortcut window"
msgid "Show Shortcuts"
msgstr ""

#: src/ui/help-overlay.blp:74
msgctxt "shortcut window"
msgid "Quit"
msgstr ""
