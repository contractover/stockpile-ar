<?xml version="1.0" encoding="UTF-8"?>
<!-- copyleft 2023 skøldis -->
<component type="desktop">
  <id>@appid@</id>
  <launchable type="desktop-id">@appid@.desktop</launchable>
  <metadata_license>CC0-1.0</metadata_license>
  <name translatable="yes">Stockpile</name>
  <summary translatable="yes">Keep count of restockable items</summary>
  <project_license>AGPL-3.0-or-later</project_license>
  <description>
    <p translatable="yes">
      Never forget to refill again! Stockpile lets you manage your
      inventory of medication, food, beauty products, or anything else.
      The app will warn you whenever you are short on something,
      so you can obtain more and not have to go empty.
    </p>
  </description>
  <content_rating type="oars-1.1"/>
  <screenshots>
    <screenshot type="default">
      <image>https://codeberg.org/turtle/stockpile/raw/branch/main/screenshots/main-view.png</image>
      <description>The main view of Stockpile</description>
    </screenshot>
    <screenshot>
      <image>https://codeberg.org/turtle/stockpile/raw/branch/main/screenshots/item-view.png</image>
      <description>The item view of Stockpile</description>
    </screenshot>
    <screenshot>
      <image>https://codeberg.org/turtle/stockpile/raw/branch/main/screenshots/edit-view.png</image>
      <description>The edit view of Stockpile</description>
    </screenshot>
  </screenshots>
  <url type="homepage">https://codeberg.org/turtle/stockpile</url>
  <url type="bugtracker">https://codeberg.org/turtle/stockpile/issues</url>
  <translation type="gettext">stockpile</translation>
  <url type="translate">https://translate.codeberg.org/engage/jellybean/</url>
  <url type="contact">https://matrix.to/#/%23stockpile-dev:turtle.garden</url>
  <releases>
    <release version="0.4.0" date="2023-12-13">
      <description>
        <ul>
          <li translatable="no">Changed app name from “Jellybean” to “Stockpile”</li>
          <li translatable="no">Updated icon to match name</li>
          <li translatable="no">Reduce user effort by removing unnessescary dialogs and toast</li>
          <li translatable="no">Display item amount in the main view</li>
          <li translatable="no">Reorder buttons and inputs across the app to make more sense</li>
          <li translatable="no">Display item icons on the item page</li>
        </ul>
        <p>
          If you are having trouble updating the app, please reinstall and
          select to keep your user data, and Stockpile should launch
          correctly afterwards.
        </p>
      </description>
    </release>
    <release version="0.3.1" date="2023-11-18">
      <description>
        <ul>
          <li translatable="no">Added Icelandic translation</li>
          <li translatable="no">Fix UI bugs</li>
        </ul>
      </description>
    </release>
    <release version="0.3.0" date="2023-11-12">
      <description>
        <ul>
          <li translatable="no">2-pane UI removed to improve clarity of use</li>
          <li translatable="no">Added feedback on jellybean delete</li>
          <li translatable="no">Icons can now be set for each item</li>
          <li translatable="no">Items can now be searched for</li>
          <li translatable="no">Fixed various bugs</li>
        </ul>
        <p translatable="no">
          This release updates the backend. Do not export
          data from this version to another version. If you were using a lower
          version, please run this version first before using a newer version.
        </p>
      </description>
    </release>
    <release version="0.2.1" date="2023-11-06">
      <description>
        <ul>
          <li translatable="no">Added German, Catalan, French, Hungarian, and Italian translations</li>
          <li translatable="no">Fix UI bugs</li>
        </ul>
      </description>
    </release>
    <release version="0.2.0" date="2023-11-03">
      <description>
        <ul translatable="no">
          <li translatable="no">Switched to 2-pane interface in larger window sizes</li>
          <li translatable="no">Removed destructive action confirmation dialogs, instead replacing them with toasts with an Undo action</li>
          <li translatable="no">Added better tooltips to many buttons and improved keyboard shortcuts</li>
          <li translatable="no">Removed the “Quick-Use” button</li>
        </ul>
      </description>
    </release>
    <release version="0.1.1" date="2023-10-30">
      <description>
        <ul translatable="no">
          <li translatable="no">Added localized window title</li>
          <li translatable="no">Adjusted icon</li>
        </ul>
      </description>
    </release>
    <release version="0.1.0" date="2023-10-29">
      <description>
        <ul translatable="no">
          <li translatable="no">Added all basic functionality</li>
          <li translatable="no">Added translation to Swedish</li>
        </ul>
      </description>
    </release>
  </releases>
  <provides>
    <binary>stockpile</binary>
  </provides>
  <requires>
    <display_length compare="ge">360</display_length>
    <internet>offline-only</internet>
  </requires>
  <reccomends>
    <control>touch</control>
    <control>pointing</control>
    <control>keyboard</control>
  </reccomends>
  <developer_name translatable="no">skøldis</developer_name>
  <update_contact>stockpile_AT_turtle.garden</update_contact>
</component>

